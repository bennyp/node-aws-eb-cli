FROM node:10-alpine
RUN npm i -g npm
RUN apk update
RUN apk add --no-cache bash
RUN apk add --no-cache git
RUN apk add --no-cache parallel
RUN apk add --no-cache python
RUN apk add --no-cache py-pip
RUN pip install awsebcli --upgrade

CMD ["bash"]


